// Q&A 1
	const a1 = document.querySelector('#q1')
	const a1Text = document.querySelector('#a1')
	a1.addEventListener('change', function(){
		let a1Value = a1[a1.selectedIndex].value;
		a1Text.textContent = "Your answer is: " + a1Value;
		if (a1Value === "wolf"){
				a1Text.textContent = 'Correct!';
				score++
			} else {
				a1Text.textContent = 'Incorrect, please try again.'
			}
		})



// Q&A 2
	const a2 = document.querySelector('#q2')
	const a2Text = document.querySelector('#a2')
	a2.addEventListener('change', function(){
		let a2Value = a2[a2.selectedIndex].value;
		a2Text.textContent = "Your answer is: " + a2Value;
		if (a2Value === "boy"){
				a2Text.textContent = 'Correct!';
				score++
			} else {
				a2Text.textContent = 'Incorrect, please try again.'
			}
		})



const firstName = document.querySelector('#first-name')

const lastName = document.querySelector('#last-name')

var score = 0;


document.getElementById('submit-button').onclick = function (){
	alert ("Good day, " + firstName.value + " " + lastName.value + ". You have " + score + "/2 questions correct.")
}